package com.explog.ru.test.di.components;

import com.explog.ru.test.di.module.FragmentModule;
import com.explog.ru.test.di.module.PresenterModule;
import com.explog.ru.test.di.scope.PerFragment;
//import com.explog.ru.english_words.main.info.view.InfoFrag/ment;
import com.explog.ru.test.main.main.view.MainFragment;
//import com.explog.ru.english_words.main.selectword.view.SelectWordFragment;

import dagger.Subcomponent;

@PerFragment
@Subcomponent(modules = {FragmentModule.class, PresenterModule.class})
public interface FragmentComponent {

//    void inject(DrawerViewFragment drawerViewFragment);


    void inject(MainFragment mainFragment);

//
//    void inject(InfoFragment infoFragment);
//
//    void inject(SelectWordFragment selectWordFragment);
//
}
