package com.explog.ru.test.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.LongSparseArray;
import android.widget.Toast;

import com.explog.ru.test.App;
import com.explog.ru.test.di.components.ConfigPersistentComponent;
import com.explog.ru.test.di.components.FragmentComponent;
import com.explog.ru.test.di.module.FragmentModule;
import com.explog.ru.test.mvp.IBaseMvpView;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Karina on 03.06.2019.
 */
public abstract class BaseMvpFragment extends BaseFragment implements IBaseMvpView {

    private static final String KEY_FRAGMENT_ID = "KEY_FRAGMENT_ID";
    private static final LongSparseArray<ConfigPersistentComponent> componentsArray = new LongSparseArray<>();
    private static final AtomicLong NEXT_ID = new AtomicLong(0);

    private long fragmentId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentId =
                savedInstanceState != null
                        ? savedInstanceState.getLong(KEY_FRAGMENT_ID)
                        : NEXT_ID.getAndIncrement();
        ConfigPersistentComponent configPersistentComponent;
        if (componentsArray.get(fragmentId) == null) {
            configPersistentComponent = App.get(getActivity()).getComponent().configPersistentComponent();
            componentsArray.put(fragmentId, configPersistentComponent);
        } else {
            configPersistentComponent = componentsArray.get(fragmentId);
        }
        FragmentComponent fragmentComponent =
                configPersistentComponent.fragmentComponent(new FragmentModule(this));
        inject(fragmentComponent);
        attachView();
    }

    protected abstract void inject(FragmentComponent fragmentComponent);

    protected abstract void attachView();

    protected abstract void detachPresenter();

    @Override
    public int getClassUniqueDeviceId() {
        return System.identityHashCode(this);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(KEY_FRAGMENT_ID, fragmentId);
    }

    @Override
    public void onDestroy() {
        if (!getActivity().isChangingConfigurations()) {
            componentsArray.remove(fragmentId);
        }
        detachPresenter();
        super.onDestroy();
    }

    @Override
    public void errorLoading(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
