package com.explog.ru.test.main.main.adapter;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.explog.ru.test.R;
import com.explog.ru.test.Urls;
import com.explog.ru.test.dataclasses.Result;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Karina on 03.06.2019.
 */
public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Result> mMovieList;
    private Pagination pagination;

    private OnItemClickListener mOnItemClickListener;

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public void setData(ArrayList<Result> movieList) {
        this.mMovieList = movieList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item, viewGroup, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (position == getItemCount() - 4) {
            pagination.needItems();
        }
        if (mMovieList != null && !mMovieList.isEmpty()) {
            Result movie = mMovieList.get(position);
            ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
            itemViewHolder.textView.setText(movie.getTitle());
            itemViewHolder.tvDescription.setText(movie.getOverview());

            //работает не совсем корректно
//            itemViewHolder.tvDescription.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    itemViewHolder.tvDescription.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    int noOfLinesVisible = itemViewHolder.tvDescription.getHeight() / itemViewHolder.tvDescription.getLineHeight();
//
//                    itemViewHolder.tvDescription.setText(movie.getOverview());
//
//                    itemViewHolder.tvDescription.setMaxLines(noOfLinesVisible);
//                    itemViewHolder.tvDescription.setEllipsize(TextUtils.TruncateAt.END);
//
//                }
//            });

            itemViewHolder.tvDate.setText(movie.getReleaseDate());

            Handler uiHandler = new Handler(Looper.getMainLooper());
            uiHandler.post(() -> Picasso.get()
                    .load(Urls.IMAGE_URL + movie.getPosterPath())
                    .into(itemViewHolder.ivPoster));
        }
    }

    @Override
    public int getItemCount() {
        return mMovieList != null ? mMovieList.size() : 0;
    }

    public interface Pagination {
        void needItems();
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        TextView tvDescription;
        TextView tvDate;
        ImageView ivPoster;

        public ItemViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            ivPoster = (ImageView) itemView.findViewById(R.id.iv_poster);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(getAdapterPosition());
                    }
                }
            });

        }
    }
}
