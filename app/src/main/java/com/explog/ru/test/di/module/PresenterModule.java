package com.explog.ru.test.di.module;

import com.explog.ru.test.main.main.IMainContract;
//import com.explog.ru.english_words.main.info.IInfoContract;
//import com.explog.ru.english_words.main.info.presenter.InfoPresenter;
import com.explog.ru.test.main.main.presenter.MainPresenter;
//import com.explog.ru.english_words.main.selectword.ISelectWordContract;
//import com.explog.ru.english_words.main.selectword.presenter.SelectWordPresenter;

import dagger.Binds;
import dagger.Module;


@Module
public abstract class PresenterModule {

    @Binds
    abstract IMainContract.Presenter bindMainPresenter(MainPresenter mainPresenter);


//    @Binds
//    abstract IInfoContract.Presenter bindInfoPresenter(InfoPresenter infoPresenter);
//
//    @Binds
//    abstract ISelectWordContract.Presenter bindSelectWordPresenter(SelectWordPresenter selectWordPresenter);

}

