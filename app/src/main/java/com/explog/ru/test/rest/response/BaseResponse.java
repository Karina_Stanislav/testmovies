package com.explog.ru.test.rest.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Karina on 02.06.2019.
 */
public class BaseResponse implements Parcelable {

    @SerializedName("status_code")
    private Integer statusCode;
    @SerializedName("status_message")
    private String statusMessage;
    @SerializedName("success")
    private Boolean success;
    @SerializedName("errors")
    private List<String> errors;

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public BaseResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.statusCode);
        dest.writeString(this.statusMessage);
        dest.writeValue(this.success);
        dest.writeStringList(this.errors);
    }

    protected BaseResponse(Parcel in) {
        this.statusCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.statusMessage = in.readString();
        this.success = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.errors = in.createStringArrayList();
    }

    public static final Creator<BaseResponse> CREATOR = new Creator<BaseResponse>() {
        @Override
        public BaseResponse createFromParcel(Parcel source) {
            return new BaseResponse(source);
        }

        @Override
        public BaseResponse[] newArray(int size) {
            return new BaseResponse[size];
        }
    };
}
