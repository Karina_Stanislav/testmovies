package com.explog.ru.test.utils;

import android.app.Activity;

import com.explog.ru.test.R;

/**
 * Created by Karina on 05.06.2019.
 */
public class UI {

    public static void animationOpenActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_in, R.anim.activity_out);
    }

    public static void animationCloseActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }


}
