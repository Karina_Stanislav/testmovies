package com.explog.ru.test.managers;

import com.explog.ru.test.request.IRequestCallbackNew;
import com.explog.ru.test.rest.response.MovieResponse;
import com.explog.ru.test.rest.service.ITestService;

import java.util.LinkedHashMap;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by Karina on 05.06.2019.
 */
public class RequestManager {

    private Retrofit mRetrofit;

    @Inject
    public RequestManager(Retrofit retrofit) {
        mRetrofit = retrofit;
    }

    private <S> S createService(Class<S> serviceClass) {
        return mRetrofit.create(serviceClass);
    }

    public <T extends Object> Disposable makeRxSingleRequest(Single<T> single, IRequestCallbackNew requestCallbackNew) {
        return makeSingleNewThreadMainSubscriber(single)
                .doOnSubscribe(disposable -> requestCallbackNew.onStartRequest())
                .doFinally(() -> requestCallbackNew.onFinishRequest())
                .subscribe(t -> requestCallbackNew.onSuccess(t), error -> requestCallbackNew.onErrorRequest(error.getLocalizedMessage()));
    }

    public <T extends Object> Single<T> makeSingleNewThreadMainSubscriber(Single<T> single) {
        return single.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
    }

    public Single<MovieResponse> getAllMovies(LinkedHashMap<String, String> params) {
        return createService(ITestService.class).getAllMovies(params);
    }

    public Disposable getAllMovies(LinkedHashMap<String, String> params, IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(getAllMovies(params), requestCallbackNew);
    }

    public Single<MovieResponse> getMoviesByQuerySingle(LinkedHashMap<String, String> params) {
        return createService(ITestService.class).getMoviesByQuerySingle(params);
    }

    public Disposable getMoviesByQuerySingle(LinkedHashMap<String, String> params, IRequestCallbackNew requestCallbackNew) {
        return makeRxSingleRequest(getMoviesByQuerySingle(params), requestCallbackNew);
    }
}
