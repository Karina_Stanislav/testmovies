package com.explog.ru.test.utils;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.explog.ru.test.App;


/**
 * Created by Karina on 04.06.2019.
 */
public class FieldConverter {

    public static String getString(int resId) {
        return App.mInstance.getResources().getString(resId);
    }

    public static String[] getStringArray(int resId) {
        return App.mInstance.getResources().getStringArray(resId);
    }

    public static int getColor(int resId) {
        return ContextCompat.getColor(App.mInstance, resId);
    }

    public static int[] getIntArray(int resId) {
        return App.mInstance.getResources().getIntArray(resId);
    }

    public static TypedArray getTypedArray(int resId) {
        return App.mInstance.getResources().obtainTypedArray(resId);
    }

    public static Drawable getDrawable(int resId) {
        return ContextCompat.getDrawable(App.mInstance, resId);
    }

    public static float getDimension(int resId) {
        return App.mInstance.getResources().getDimension(resId);
    }
}