package com.explog.ru.test.screencreator;

import android.os.Bundle;

import com.explog.ru.test.fragment.BaseFragment;

public interface IScreenCreator {

    <T extends BaseFragment> T newInstance(Class<T> mClass);

    <T extends BaseFragment> T newInstance(Class<T> mClass, Bundle bundle);

}
