package com.explog.ru.test;

/**
 * Created by Karina on 03.06.2019.
 */
public class Urls {

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String IMAGE_URL = "https://image.tmdb.org/t/p/w342";

    public static final String DISCOVER_MOVIE = "discover/movie";
    public static final String SEARCH_MOVIE = "search/movie";

}
