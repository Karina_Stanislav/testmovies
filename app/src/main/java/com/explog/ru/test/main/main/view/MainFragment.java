package com.explog.ru.test.main.main.view;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Surface;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.explog.ru.test.R;
import com.explog.ru.test.dataclasses.Result;
import com.explog.ru.test.di.components.FragmentComponent;
import com.explog.ru.test.fragment.BaseMvpFragment;
import com.explog.ru.test.main.main.IMainContract;
import com.explog.ru.test.main.main.adapter.MainAdapter;
import com.explog.ru.test.utils.FieldConverter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Karina on 03.06.2019.
 */
public class MainFragment extends BaseMvpFragment implements IMainContract.View, MainAdapter.Pagination, MainAdapter.OnItemClickListener {

    @BindView(R.id.rv_list)
    RecyclerView rvList;
    RecyclerView.LayoutManager layoutManager;
    @BindView(R.id.rl_search)
    RelativeLayout rlSearch;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.tv_empty_search)
    TextView tvEmptySearch;
    @BindView(R.id.ll_empty_search)
    LinearLayout llEmptySearch;
    @BindView(R.id.iv_reload)
    ImageView ivReload;
    @BindView(R.id.ll_error)
    RelativeLayout llError;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    MainAdapter mainAdapter = new MainAdapter();

    @Inject
    IMainContract.Presenter mPresenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.onCreate(savedInstanceState);
        initAdapters();
        setListener();
    }

    private void setListener() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    etSearch.setCursorVisible(false);
            }
        });
        PublishSubject<String> querySubj = PublishSubject.create();
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ivClose.setVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
                querySubj.onNext(s.toString());
            }
        });
        mPresenter.setSearchQuerySubject(querySubj);
        ivClose.setOnClickListener(v -> etSearch.setText(""));
    }

    private void initAdapters() {
        if (mActivity.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_0
                || mActivity.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_180) {
            layoutManager = new GridLayoutManager(mActivity, 1, LinearLayoutManager.VERTICAL, false);
        } else {
            layoutManager = new GridLayoutManager(mActivity, 2, LinearLayoutManager.VERTICAL, false);
        }
        mainAdapter.setOnItemClickListener(this);
        rvList.setLayoutManager(layoutManager);
        mainAdapter.setPagination(this);
        rvList.setAdapter(mainAdapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int mScrollPosition = ((GridLayoutManager) layoutManager).findFirstVisibleItemPosition();
        if (mActivity.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_0
                || mActivity.getWindowManager().getDefaultDisplay().getRotation() == Surface.ROTATION_180) {
            layoutManager = new GridLayoutManager(mActivity, 1, LinearLayoutManager.VERTICAL, false);
        } else {
            layoutManager = new GridLayoutManager(mActivity, 2, LinearLayoutManager.VERTICAL, false);

        }
        layoutManager.scrollToPosition(mScrollPosition);
        rvList.setLayoutManager(layoutManager);
    }

    @OnClick(R.id.iv_reload)
    public void onClick() {
        if (etSearch.getText().toString().isEmpty()) {
            mPresenter.getMoviesList();
        } else {
            mPresenter.getMoviesListByTitle(etSearch.getText().toString());
        }
    }


    @Override
    public void showData(ArrayList<Result> movieList) {
        if (movieList.isEmpty() && !etSearch.getText().toString().isEmpty()) {
            tvEmptySearch.setText(String.format(FieldConverter.getString(R.string.search_text_empty), etSearch.getText().toString()));
            emptyLoading();
        } else {
            successLoading();
            rvList.getRecycledViewPool().clear();
            mainAdapter.setData(movieList);
        }
    }

    @Override
    public void itemsAdded() {
//        rvList.getRecycledViewPool().clear();
        mainAdapter.notifyDataSetChanged();
    }

    @Override
    public void needItems() {
        if (etSearch.getText().toString().isEmpty()) {
            mPresenter.getMoviesList();
        } else {
            mPresenter.getMoviesListByTitle(etSearch.getText().toString());
        }
    }

    public void emptyLoading() {
        progressBar.setVisibility(View.GONE);
        llError.setVisibility(View.GONE);
        llEmptySearch.setVisibility(View.VISIBLE);
        rvList.setVisibility(View.GONE);
    }

    @Override
    public void startLoading() {
        progressBar.setVisibility(View.VISIBLE);
        llError.setVisibility(View.GONE);
        llEmptySearch.setVisibility(View.GONE);
        rvList.setVisibility(View.GONE);
    }

    @Override
    public void errorLoading() {
        progressBar.setVisibility(View.GONE);
        llError.setVisibility(View.VISIBLE);
        llEmptySearch.setVisibility(View.GONE);
        rvList.setVisibility(View.GONE);
    }

    @Override
    public void successLoading() {
        progressBar.setVisibility(View.GONE);
        if (rvList.getVisibility() != View.VISIBLE) {
            llError.setVisibility(View.GONE);
            llEmptySearch.setVisibility(View.GONE);
            rvList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void errorLoadingWithSnack() {
        // SnackBar
    }


    @Override
    public void onItemClick(int position) {
        mPresenter.openToastForMovie(etSearch.getText().toString().isEmpty(), position);
    }

    @Override
    public void showToastForMovie(String titleMovie) {
        Toast.makeText(mActivity, titleMovie, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    protected void attachView() {
        mPresenter.attachView(this);
    }

    @Override
    protected void detachPresenter() {
        mPresenter.detachView();
    }


    @Override
    protected int getLayout() {
        return R.layout.fragment_main;
    }

}
