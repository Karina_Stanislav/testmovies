package com.explog.ru.test;

import java.util.LinkedHashMap;

/**
 * Created by Karina on 04.06.2019.
 */
public class RequestParams {

    private static final String RU_LOCALE = "ru-RU";

    public static LinkedHashMap<String, String> getMoviesParams(String page, String apiKey, String query) {
        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put(RequestField.PAGE, page);
        params.put(RequestField.LANGUAGE, RU_LOCALE);
        params.put(RequestField.API_KEY, apiKey);
        if (query != null && !query.isEmpty()) {
            params.put(RequestField.QUERY, query);
        }
        return params;
    }

}
