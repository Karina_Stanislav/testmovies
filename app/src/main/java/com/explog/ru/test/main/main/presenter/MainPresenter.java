package com.explog.ru.test.main.main.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.explog.ru.test.Actions;
import com.explog.ru.test.R;
import com.explog.ru.test.RequestParams;
import com.explog.ru.test.dataclasses.Result;
import com.explog.ru.test.eventbus.Event;
import com.explog.ru.test.eventbus.EventSuccess;
import com.explog.ru.test.main.main.IMainContract;
import com.explog.ru.test.mvp.BaseEventBusPresenter;
import com.explog.ru.test.rest.response.MovieResponse;
import com.explog.ru.test.utils.FieldConverter;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Karina on 03.06.2019.
 */
public class MainPresenter extends BaseEventBusPresenter<IMainContract.View> implements IMainContract.Presenter {

    public static final String SAVE_MOVIES = "MainPresenter.SAVE_MOVIES";

    private ArrayList<Result> mMovieList;
    private ArrayList<Result> mSearchedMovieList;
    private Disposable mQueryDisposable;
    private Disposable mDisposable;
    private int page = 1;
    private int pageSearch = 1;
    private int totalPage = 1;
    private int totalPageSearch = 1;
    private String mSearchQuery = "";

    @Inject
    public MainPresenter() {
        mDisposable = Disposables.empty();
        mQueryDisposable = Disposables.empty();
        mMovieList = new ArrayList<>();
        mSearchedMovieList = new ArrayList<>();
    }

    @Override
    public void saveInstanceState(Bundle outState) {
        super.saveInstanceState(outState);
        outState.putParcelableArrayList(SAVE_MOVIES, mMovieList);
    }

    @Override
    public void restoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.restoreInstanceState(savedInstanceState);
        mMovieList = savedInstanceState.getParcelableArrayList(SAVE_MOVIES);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getMoviesList();
        } else {
            restoreInstanceState(savedInstanceState);
        }
    }

    @Override
    public void getMoviesList() {
        mSearchedMovieList.clear();
        if (page == 1 && !mMovieList.isEmpty()) {
            getMvpView().showData(mMovieList);
            page++;
        } else {
            if (page <= totalPage) {
                mDisposable = getRequestManager().getAllMovies(RequestParams.getMoviesParams(String.valueOf(page), FieldConverter.getString(R.string.api_key), null),
                        getRequestController(Actions.GET_ALL_MOVIES));
                addDisposable(mDisposable);
            }
        }
    }

    @Override
    public void setSearchQuerySubject(PublishSubject<String> querySubject) {
        mQueryDisposable = querySubject.debounce(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (s.isEmpty()) {
                        if (!mMovieList.isEmpty()) {
                            getMvpView().showData(mMovieList);
                        } else {
                            getMoviesList();
                        }
                    } else {
                        getMoviesListByTitle(s);
                    }

                });
        addDisposable(mQueryDisposable);
    }

    @Override
    public void getMoviesListByTitle(String searchQuery) {
        if (!mSearchQuery.equalsIgnoreCase(searchQuery)) {
            pageSearch = 1;
            mSearchedMovieList.clear();
            mSearchQuery = searchQuery;
        }
        if (pageSearch <= totalPageSearch) {
            mDisposable = getRequestManager().getMoviesByQuerySingle(RequestParams.getMoviesParams(String.valueOf(pageSearch++), FieldConverter.getString(R.string.api_key), searchQuery),
                    getRequestController(Actions.GET_MOVIES_BY_QUERY));
            addDisposable(mDisposable);
        }
    }

    @Override
    public void openToastForMovie(boolean searchQueryIsEmpty, int position) {
        if (searchQueryIsEmpty) {
            getMvpView().showToastForMovie(mMovieList.get(position).getTitle());
        } else {
            getMvpView().showToastForMovie(mSearchedMovieList.get(position).getTitle());
        }
    }

    @Override
    public void onEvent(Event event) {
        if (event.getmClassUniqueId() == getMvpView().getClassUniqueDeviceId()) {
            switch (event.getmEventType()) {
                case SUCCESS_REQUEST:
                    switch (event.getActionCode()) {
                        case Actions.GET_ALL_MOVIES:
                            undisposable(mDisposable);
                            MovieResponse movieResponse = (MovieResponse) ((EventSuccess) event).getData();
                            mMovieList.addAll(movieResponse.getResults());
                            totalPage = movieResponse.getTotalPages();
                            page = movieResponse.getPage() + 1;
                            getMvpView().showData(mMovieList);
                            pageSearch = 1;
                            break;
                        case Actions.GET_MOVIES_BY_QUERY:
                            undisposable(mDisposable);
                            MovieResponse movieSearchResponse = (MovieResponse) ((EventSuccess) event).getData();
                            totalPageSearch = movieSearchResponse.getTotalPages();
                            if (!mSearchedMovieList.isEmpty()) {
                                getMvpView().successLoading();
                                mSearchedMovieList.addAll(movieSearchResponse.getResults());
                                getMvpView().itemsAdded();
                            } else {
                                mSearchedMovieList.addAll(movieSearchResponse.getResults());
                                getMvpView().showData(mSearchedMovieList);
                            }
                            break;
                    }

                    break;
                case FAIL_REQUEST:
                    undisposable(mDisposable);
                    switch (event.getActionCode()) {
                        case Actions.GET_ALL_MOVIES:
                            if (mMovieList.isEmpty()) {
                                getMvpView().errorLoading();
                                page = 1;
                            } else {
                                getMvpView().itemsAdded();
                                getMvpView().errorLoadingWithSnack();
                            }

                            break;
                        case Actions.GET_MOVIES_BY_QUERY:
                            if (mSearchedMovieList.isEmpty()) {
                                getMvpView().errorLoading();
                                pageSearch = 1;
                            } else {
                                getMvpView().itemsAdded();
                                getMvpView().errorLoadingWithSnack();
                                pageSearch--;
                            }
                            break;
                    }
                    break;
            }
        }
    }
}
