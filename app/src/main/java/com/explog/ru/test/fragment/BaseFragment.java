package com.explog.ru.test.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.explog.ru.test.activity.BaseActivity;
import com.explog.ru.test.screencreator.ScreenCreator;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Karina on 03.06.2019.
 */
public abstract class BaseFragment extends Fragment {

    protected Activity mActivity;
    protected boolean isSavedInstanceStateDone;
    protected boolean isMakeStartRequest;
    private Unbinder mUnbinder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            mActivity = (Activity) context;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            mActivity = activity;
        }
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    protected ScreenCreator getScreenCreator() {
        return ScreenCreator.getInstance();
    }


    protected abstract int getLayout();

    protected void retry() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        isSavedInstanceStateDone = false;

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isSavedInstanceStateDone = true;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public boolean isSavedInstanceStateDone() {
        return isSavedInstanceStateDone;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        mActivity = null;
    }


}