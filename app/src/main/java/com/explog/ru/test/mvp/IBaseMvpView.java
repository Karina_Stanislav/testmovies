package com.explog.ru.test.mvp;


public interface IBaseMvpView {

    int getClassUniqueDeviceId();

    void errorLoading(String message);


}
