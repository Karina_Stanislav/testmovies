package com.explog.ru.test.di.components;

import com.explog.ru.test.di.module.FragmentModule;

import dagger.Subcomponent;

@Subcomponent
public interface ConfigPersistentComponent {

    FragmentComponent fragmentComponent(FragmentModule fragmentModule);

}
