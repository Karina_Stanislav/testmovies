package com.explog.ru.test;

/**
 * Created by Karina on 04.06.2019.
 */
public class RequestField {

    public static final String PAGE = "page";
    public static final String API_KEY = "api_key";
    public static final String LANGUAGE = "language";
    public static final String QUERY = "query";


}
