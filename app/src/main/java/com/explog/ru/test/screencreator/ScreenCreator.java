package com.explog.ru.test.screencreator;

import android.os.Bundle;

import com.explog.ru.test.fragment.BaseFragment;

public class ScreenCreator implements IScreenCreator {

    private static ScreenCreator mInstance;

    private ScreenCreator() {

    }

    public static ScreenCreator getInstance() {
        if (mInstance == null) {
            mInstance = new ScreenCreator();
        }
        return mInstance;
    }

    public <T extends BaseFragment> T newInstance(Class<T> mClass) {
        return newInstance(mClass, null);
    }

    public <T extends BaseFragment> T newInstance(Class<T> mClass, Bundle bundle) {
        try {
            T instance = mClass.newInstance();
            if (bundle != null) {
                instance.setArguments(bundle);
            }
            return instance;
        } catch (InstantiationException e) {
            throw new FragmentNotCreateException();
        } catch (IllegalAccessException e) {
            throw new FragmentNotCreateException();
        }
    }

    private static class FragmentNotCreateException extends RuntimeException {
        FragmentNotCreateException() {
            super("OffersListFragment not created");
        }
    }

}
