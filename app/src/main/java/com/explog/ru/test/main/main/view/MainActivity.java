package com.explog.ru.test.main.main.view;

import android.os.Bundle;

import com.explog.ru.test.R;
import com.explog.ru.test.activity.BaseActivity;

/**
 * Created by Karina on 03.06.2019.
 */
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            openFragment();
        } else {
            onRestoreInstanceState(savedInstanceState);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.app_bar_main;
    }

    @Override
    protected void openFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, getScreenCreator().newInstance(MainFragment.class)).commitAllowingStateLoss();
    }
}
