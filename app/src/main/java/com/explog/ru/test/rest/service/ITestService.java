package com.explog.ru.test.rest.service;

import com.explog.ru.test.Urls;
import com.explog.ru.test.rest.response.MovieResponse;

import java.util.LinkedHashMap;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Karina on 03.06.2019.
 */
public interface ITestService {

    @GET(Urls.DISCOVER_MOVIE)
    Single<MovieResponse> getAllMovies(@QueryMap LinkedHashMap<String, String> params);

    @GET(Urls.SEARCH_MOVIE)
    Single<MovieResponse> getMoviesByQuerySingle(@QueryMap LinkedHashMap<String, String> params);

}
