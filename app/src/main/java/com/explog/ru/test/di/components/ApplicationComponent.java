package com.explog.ru.test.di.components;

import com.explog.ru.test.di.module.ApplicationModule;
import com.explog.ru.test.di.module.NetModule;
import com.explog.ru.test.managers.DataManager;
import com.explog.ru.test.managers.RequestManager;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetModule.class})
public interface ApplicationComponent {

    ConfigPersistentComponent configPersistentComponent();

    DataManager dataManager();

    RequestManager requestmanager();

}
