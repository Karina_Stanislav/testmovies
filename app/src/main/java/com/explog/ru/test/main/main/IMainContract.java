package com.explog.ru.test.main.main;

import com.explog.ru.test.dataclasses.Result;
import com.explog.ru.test.mvp.IBaseMvpPresenter;
import com.explog.ru.test.mvp.IBaseMvpView;

import java.util.ArrayList;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by Karina on 03.06.2019.
 */
public interface IMainContract {

    interface View extends IBaseMvpView {

        void showData(ArrayList<Result> movieList);

        void itemsAdded();

        void startLoading();

        void errorLoading();

        void successLoading();

        void errorLoadingWithSnack();

        void showToastForMovie(String titleMovie);
    }

    interface Presenter extends IBaseMvpPresenter<View> {

        void getMoviesList();

        void getMoviesListByTitle(String searchQuery);

        void setSearchQuerySubject(PublishSubject<String> querySubject);

        void openToastForMovie(boolean empty, int position);
    }

}
