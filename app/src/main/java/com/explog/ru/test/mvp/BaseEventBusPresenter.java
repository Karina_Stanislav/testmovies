package com.explog.ru.test.mvp;

import com.explog.ru.test.App;
import com.explog.ru.test.eventbus.Event;
import com.explog.ru.test.eventbus.EventBusController;
import com.explog.ru.test.eventbus.IEventBusObserver;
import com.explog.ru.test.managers.RequestManager;
import com.explog.ru.test.request.RequestController;

import javax.inject.Inject;

public abstract class BaseEventBusPresenter<T extends IBaseMvpView> extends BasePresenter<T> implements IEventBusObserver {

    @Inject
    protected EventBusController mEventBusController;
    private RequestManager mRequestManager;

    @Override
    public void attachView(T mvpView) {
        super.attachView(mvpView);
        mEventBusController.addObserver(this);
    }

    @Override
    public void detachView() {
        super.detachView();
        mEventBusController.removeObserver(this);
    }

    protected RequestManager getRequestManager() {
        if (mRequestManager == null) {
            mRequestManager = App.mInstance.getComponent().requestmanager();
        }
        return mRequestManager;
    }

    protected RequestController getRequestController(int actionCode) {
        return new RequestController(mEventBusController, actionCode, getMvpView().getClassUniqueDeviceId());
    }


    @Override
    public void onStartEvent(Event event) {

    }

    @Override
    public void onFinishEvent(Event event) {

    }

    @Override
    public void onEvent(Event event) {

    }
}
